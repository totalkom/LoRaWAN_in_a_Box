#!/usr/bin/env python3

import logging
logging.basicConfig(
   level=logging.DEBUG,
   format='%(asctime)s:%(levelname)s:%(name)s:%(message)s',
   filename="/home/pi/out.log",
   filemode='a'
)
logger = logging.getLogger()
print = logger.info

import paho.mqtt.client as mqtt
import datetime
import time
from influxdb import InfluxDBClient
import json
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    client.subscribe("application/#")
    
def on_message(client, userdata, msg):
    print("Received a message on topic: " + msg.topic)
    
   # Use utc as timestamp
    receiveTime=datetime.datetime.utcnow().isoformat(' ')
    message=msg.payload.decode("utf-8")
    
    message=json.loads(message)    
    receivedData=message["object"]

    # print(message["applicationName"])
    
       
    # print(str(receiveTime) + ": " + msg.topic + " " + str(val))
    print("storing data")
    json_body = [
        {
            "measurement": message["applicationName"],
            "time":receiveTime,
            "fields": receivedData            
            }
        ]
    
    print(json_body)
    try:
        dbclient.write_points(json_body)
    except:
        print("error")

    print("Finished writing to InfluxDB")
        
# Set up a client for InfluxDB
dbclient = InfluxDBClient('localhost', 8086, 'root', 'root', 'databasein')
print(dbclient)
# Initialize the MQTT client that should connect to the Mosquitto broker
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
connOK=False
while(connOK == False):
    print("trying to connect")
    try:
        client.connect("localhost", 1883, 60)
        connOK = True
        print("connected")
    except:
        print("not connected")
        connOK = False
    time.sleep(2)

# Blocking loop to the Mosquitto broker
client.loop_forever()
