#!/bin/bash


sudo apt update

sudo apt install apt-transport-https curl python3-pip -y

if [ $? != 0 ]; then
    echo -e "\e[31mInstallation Failed. Please run the script again \e[0m"
    exit
else
    echo -e "\e[32mUpdated and Installed Curl and Pip \e[0m"
fi

pip3 install paho-mqtt

if [ $? != 0 ]; then
    echo -e "\e[31mPaho-MQTT Installation Failed. Please run the script again \e[0m"
    exit
else
    echo -e "\e[32mPaho-MQTT Installation Successful \e[0m"
fi

pip3 install influxdb

if [ $? != 0 ]; then
    echo -e "\e[31mInfluxdb Installation Failed. Please run the script again \e[0m"
    exit
else
    echo -e "\e[32mInfluxDB Installation Successful \e[0m"
fi

curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -source /etc/os-release

curl https://bintray.com/user/downloadSubjectPublicKey?username=bintray | sudo apt-key add -


test $VERSION_ID = "8" && echo "deb https://repos.influxdata.com/debian jessie stable" | sudo tee /etc/apt/sources.list.d/influxdb.list

echo "deb https://dl.bintray.com/fg2it/deb jessie main" | sudo tee -a /etc/apt/sources.list.d/grafana.list

sudo apt update

sudo apt-get install influxdb -y

sudo service influxdb start




##sudo apt install apt-transport-https curl

##curl https://bintray.com/user/downloadSubjectPublicKey?username=bintray | sudo apt-key add -

##echo "deb https://dl.bintray.com/fg2it/deb jessie main" | sudo tee -a /etc/apt/sources.list.d/grafana.list

##sudo apt update

sudo apt install grafana -y

sudo service grafana-server start

sudo update-rc.d grafana-server defaults

sudo systemctl enable grafana-server.service

sudo apt-get install gnome-schedule

#sudo mkdir /opt/mqtt_api

#sudo cp mqtt_api_influxdb.py /opt/mqtt_api/

##sudo echo "@reboot python3 /opt/mqtt_api/mqtt_api_influxdb.py" >> /var/spool/cron/crontabs/pi

echo -e "\e[32mRebooting System Now \e[0m"

sudo reboot

###sudo cp mqtt-influx-push.service /lib/systemd/system/
##sudo systemctl start mqtt-influx-push.service
##sudo systemctl enable mqtt-influx-push.service

##sudo update-rc.d mqtt-influx-push.service defaults
